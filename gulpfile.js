var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    pug          = require('gulp-pug'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin     = require('gulp-imagemin'),
    minify       = require('gulp-minify-css'),
    uglify       = require('gulp-uglify'),
    babel        = require('gulp-babel'),
    browserSync  = require('browser-sync').create(),
    runSequence  = require('run-sequence'),
    del          = require('del'),
    cache        = require('gulp-cache'),
    notify       = require('gulp-notify'),
    concat       = require('gulp-concat');

  //Con-cat nối file
    gulp.task('concat-js',function(){
      return gulp.src([
        "bower_components/jquery/dist/jquery.js",
        "bower_components/popper.js/umd/popper.js",
        "node_modules/bootstrap/dist/js/bootstrap.js",
        "bower_components/readmore/readmore.js",
        "bower_components/flexslider/jquery.flexslider.js",
        "bower_components/wow/wow.js",
        "bower_components/rating/jquery.barrating.js",
        "bower_components/matchHeight/jquery.matchHeight-min.js",
        "bower_components/mCustomScrollbar/jquery.mCustomScrollbar.min.js",
        "bower_components/swiper/js/swiper.min.js",
      ])
      .pipe(uglify())
      .pipe(concat('core.js'))
      .pipe(gulp.dest('./dist/js'));
    });
    gulp.task('concat-css',function(){
      return gulp.src([
      	"node_modules/bootstrap/dist/css/bootstrap.css",
        "bower_components/animate.css/animate.css",
        "bower_components/font-awesome/css/font-awesome.css",
        "bower_components/mCustomScrollbar/jquery.mCustomScrollbar.min.css",
        "bower_components/swiper/css/swiper.min.css",
        "bower_components/flexslider/flexslider.css",
        "bower_components/aos/dist/aos.css"
      ])
      .pipe(concat('core.min.css'))
      .pipe(minify())
      .pipe(gulp.dest('./dist/css'));
    });
    // Setup Sass
    gulp.task('sass', function () {
        return gulp.src('./src/sass/*.scss') // In
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browers: ['last 2 versions','last 15 versions', '> 1%', 'ie 8', 'ie 7']
        }))
        .pipe(sass({
          pretty: true
        }))
        .pipe(sourcemaps.write('./map')) // debug easy
        .pipe(gulp.dest('./dist/css')) // Out
        .pipe(browserSync.stream());

      });
      // Js
     gulp.task('js', function(){
         gulp.src('./src/script/*.js')
         .pipe(babel({
          presets:[
          ['env', {modules: false}]
          ]
          }))
	.pipe(gulp.dest('./dist/js'))
      })
      // Setup Pug
    gulp.task('pug', function buildHTML () {
        return gulp.src([
          './src/**/*.pug',
          '!./src/template/**/*.pug'
        ]) // In
        .pipe(pug({
          pretty: true
        }))
        .pipe(gulp.dest('./dist')) // Out
        .pipe(browserSync.stream());
      });

      // Setup image
      gulp.task('img', function() {
        return gulp.src('src/img/**/*')
          .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
          .pipe(gulp.dest('dist/images'))
          .pipe(notify({ message: 'Images task complete' }));
      });
    // FONTS
    gulp.task('fonts', function () {
      gulp.src('./src/fonts/*.*')
      .pipe(gulp.dest('./dist/fonts'))
    });
      // browserSync
    gulp.task('serve', ['sass', 'pug','img'], function () {
      browserSync.init({
        server: {
          baseDir : './dist'
        }
      });

      gulp.watch("./src/sass/**/*.scss", ['sass']);
      gulp.watch("./src/**/*.pug", ['pug']);
      gulp.watch("./src/js/**/*.js",['js']);
      gulp.watch("./src/img/*",['img']);
      gulp.watch("dist/**/*.html").on('change', browserSync.reload);
    });
    // delete
    gulp.task('clean', function () {
      return del(['dist'])
    });
    // default
     gulp.task('default', function (cb) {
      runSequence(
        'clean',
        'sass',
        'js',
        'pug',
        'serve',
        'img',
        'concat-js',
        'concat-css',
        'fonts',
        cb);
    });