﻿(function() {
  var bgSection = function(){
    $('[data-bg]').each(function(index, el) {
      var bg = $(this).data('bg');
      $(this).css('background-image', "url('"+ bg +"')");
    });
  };
  var dataSlider;
  var swipers = []; // khoi tao chuoi slider
  var images = "";
  var json = [
    "./images/tu_lanh_nho.png",
    "./images/tu_lanh_nho.png",
    "./images/tu_lanh_nho.png",
  ];

  window.inputNumber = function(el) {

      var min = el.attr('min') || false;
      var max = el.attr('max') || false;

      var els = {};

      els.dec = el.prev();
      els.inc = el.next();

      el.each(function() {
        init($(this));
      });

      function init(el) {

        els.dec.on('click', decrement);
        els.inc.on('click', increment);

        function decrement() {
          var value = el[0].value;
          value--;
          if(!min || value >= min) {
            el[0].value = value;
          }
        }

        function increment() {
          var value = el[0].value;
          value++;
          if(!max || value <= max) {
            el[0].value = value++;
          }
        }
      }
    }

  var accordionRadio = function(selector,input){
    $(selector).slideUp();
      var x = true;
      $(input).change(function() {
       if(this.checked && x == true) {
         $(selector).slideDown();  
          x = false;
       }
       else if(this.checked && x == false){
          $(selector).slideUp();
          x = true; 
       }
   });
  }  
  var btnResponsiveFix = function(selector,btn){
    $(btn).on('click', function(event) {
      var id = $(this).data('responsive');
      if (btn == ".btn-responsive--left") {
        $(id).addClass('show-left');
      }
      else if (btn == ".btn-responsive--right"){
        $(id).addClass('show-right');
      }
    });
    $(selector).on('click', '.btn--close', function(event) {
      if (btn == ".btn-responsive--left") {
        $(selector).removeClass('show-left');
      }
      else if (btn == ".btn-responsive--right"){
        $(selector).removeClass('show-right');
      }
    });
    $(selector).on('click', '.overlay', function(event) {
      console.log('fdsfdsfds');
      if (btn == ".btn-responsive--left") {
        $(selector).removeClass('show-left');
      }
      else if (btn == ".btn-responsive--right"){
        $(selector).removeClass('show-right');
      }
    });
  }
 

  var hoverNav = function(selector) {
    $(selector).on('mouseover', function(event) {
      $(selector).removeClass('active');
      $(this).addClass('active');
    });
  }

  var sidebarLocal = function() {
    $('.sidebar-local').fadeOut();
    $('.change-local').on('click', function(event) {
      $('.sidebar-local').fadeIn();
      return false;
    });
    var n=0;
    $('.local').on('change', function(event) {
      n++;
      if (n % 2 == 0) {
        $('.sidebar-local').fadeOut();
        var txt = [];
        $( ".local option:selected" ).each(function() {
          var va = $(this).val();
          txt +=  " " + va;
        });
        $('.local-option').text(txt);
      }
    });
    
  }
  var getJson = function(json) {
    $.each(json,function(i,image){
      images += '<div class="swiper-slide"> <img src="'+ image +'" class="img-responsive" alt="" ></div>';
    });
    if (images == "") {
      return false;
    }
  }
  var navIcon = function(selector){
    $(selector).click(function(){
      $(this).toggleClass('open');
    });
  }
  var hoverProduct = function(json) {
    $('.product-small').on('mouseover', function(event) {
      getJson(json);
      if ($(this).find('.swiper-wrapper .swiper-slide').length == 0) {

        $(this).find('.swiper-wrapper').append(images);
        getInfoSlider();
        $(this).find('.loading').css('display', 'none');
      }
    });
  }
  var getInfoSlider = function(){
    $('.create-slider').each(function(index, el) {
      dataSlider = $(this).data('slider').split(",");
      var swiper = null;
      if (dataSlider.length <= 1) {

         swiper = new Swiper(dataSlider[0], {
          slidesPerView: 1,
          spaceBetween: 30,
          loop: true,
          pagination: {
            el:dataSlider[0] + ' .swiper-pagination',
            clickable: true,
          },
          navigation: {
            nextEl:dataSlider[0] + '-next',
            prevEl:dataSlider[0] + '-prev',
          }
        });
      }
      else if (dataSlider.length == 3){
          swiper = new Swiper(dataSlider[0], {
          slidesPerView: Number(dataSlider[1]),
          spaceBetween: Number(dataSlider[2]),
          navigation: {
            nextEl:dataSlider[0] + '-next',
            prevEl:dataSlider[0] + '-prev',
          },
        });

      }
      else{
         swiper = new Swiper(dataSlider[0], {
          slidesPerView: Number(dataSlider[1]),
          spaceBetween: Number(dataSlider[5]),
          navigation: {
            nextEl:dataSlider[0] + '-next',
            prevEl:dataSlider[0] + '-prev',
          },
          breakpoints: {
            // when window width is <= 320px
            480: {
              slidesPerView: Number(dataSlider[4]),
              spaceBetween: 10
            },
            // when window width is <= 480px
            768: {
              slidesPerView: Number(dataSlider[3]),
              spaceBetween: 20
            },
            // when window width is <= 640px
            1024: {
              slidesPerView: Number(dataSlider[2]),
              spaceBetween: 30
            }
          }
        });
      }
      swipers.push(swiper);
    });   
  }
  var menuFix = function(selector){
    var menu = "show";
    var hmenu = $(selector).height();
    $('body').css('padding-top', hmenu);
    $(window).scroll(function(event) {
        /* Act on the event */
        if(window.pageYOffset > hmenu ){
          if (menu == "show") {
            menu = "hidden";
            $(selector).addClass('show-nav');
          }
        }
        else if (window.pageYOffset < hmenu){
          if (menu == "hidden") {
            $(selector).removeClass('show-nav');
            menu = "show";
          }
        }
    });
  }
  var scrolTabDetail = function() {
    
  }
  var activeMenu = function(selector) {
    $(selector).on('click', '.accordion-toggle', function(event) {
      $(this).parent('li').toggleClass('active');
    });
  }
  var dataSlider1;
  var createGallery = function(selector1){
    $(selector1).each(function(index, el) {
       dataSlider1 = $(this).data('slide').split(",");
      $(dataSlider1[1]).flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: true,
        itemWidth: 65,
        itemMargin: 30,
        asNavFor: dataSlider1[0],
      });

      $(dataSlider1[0]).flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: dataSlider1[1],
      });

    });
  }
    
  $(document).ready(function() {
    inputNumber($('.input-number'));
    sidebarLocal();
    bgSection();
    getInfoSlider();
    hoverProduct(json);
    btnResponsiveFix('.repsonsive-left','.btn-responsive--left');
    btnResponsiveFix('.repsonsive-right','.btn-responsive--right');
    accordionRadio('.form-new__address','input[type="radio"][name="old-address"]');
    createGallery('.product-gallery');
    hoverNav('.nav-hover');
    navIcon('.nav-icon');
    activeMenu('.menu-responsive');
    menuFix('.header-default');



    // $('.info__content').readmore({
    //   moreLink: '<a href="#">Usage, examples, and options</a>',
    //   collapsedHeight: 384,
    //   afterToggle: function(trigger, element, expanded) {
    //     if(! expanded) { // The "Close" link was clicked
    //       $('html, body').animate({scrollTop: element.offset().top}, {duration: 100});
    //     }
    //   }
    // });
    $(".specifications__content").mCustomScrollbar({
      theme:"minimal-dark"
    });
    $('.example-bootstrap').barrating({
        theme: 'bootstrap-stars',
        showSelectedRating: false
    });
    $(document).on( 'shown.bs.tab', 'a[data-toggle=\'tab\']', function (e) {

      $('article').readmore({
        speed: 500,
        collapsedHeight:510,
        moreLink: `<div class="text-center">      
                      <a href="" class="btn btn--secondary mt-2 btn--readmore"> Xem Thêm</a>
                  </div>`,
        lessLink: `<div class="text-center">      
                      <a href="" class="btn btn--secondary mt-2 btn--readmore"> Đóng</a>
                  </div>`,
        afterToggle: function() {
            $('.block-shadow').toggleClass('hidden');
        }          
      });
    })
   
    $('.flex-next').addClass('icon-next');
    $('.flex-prev').addClass('icon-prev');
  });


  (function(a) {
        (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    })(navigator.userAgent || navigator.vendor || window.opera);

    // hien thi ipad
    var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
  
    // hiển thị mobie
    if (jQuery.browser.mobile) {
     
    }
    // hiển thị ipad
    else if (isiPad) {
      

    }
    // hiển thị destop
    else {
        var removeHidden = function(selector) {
          $(selector).each(function(index, el) {
            $(this).removeClass('hidden');
          });
        }
        var removeShow = function(selector){
          $(selector).each(function(index, el) {
            $(this).removeClass('show');
          });
        }
        $(document).ready(function() {
          removeHidden('.hidden-responsive');
          removeShow('.show-responsive');
        });
    }



})(); 